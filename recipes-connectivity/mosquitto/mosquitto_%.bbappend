FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " \
    file://hivle.conf \
    file://hivle.passwordfile \
"

FILES:${PN} += " \
    ${sysconfdir}/mosquitto/conf.d/* \
"

do_install:append() {
    install -d ${D}${sysconfdir}/mosquitto/conf.d
    install -m 0644 ${WORKDIR}/hivle.conf ${D}${sysconfdir}/mosquitto/conf.d/
    install -m 0644 ${WORKDIR}/hivle.passwordfile ${D}${sysconfdir}/mosquitto/conf.d/

    sed -i 's|#include_dir|include_dir /etc/mosquitto/conf.d|' ${D}${sysconfdir}/mosquitto/mosquitto.conf
}