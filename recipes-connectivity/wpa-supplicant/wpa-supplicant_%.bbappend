FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SYSTEMD_SERVICE:${PN}:append = " wpa_supplicant-nl80211@wlan0.service"
SYSTEMD_AUTO_ENABLE = "enable"

SRC_URI += " \
    file://wpa_supplicant-nl80211-wlan0.conf \
"

FILES:${PN} += " \
    ${sysconfdir}/wpa_supplicant/wpa_supplicant-nl80211-wlan0.conf \
"

do_install:append:hivle() {
    install -d ${D}${sysconfdir}/wpa_supplicant/
    install -D -m 0644 ${WORKDIR}/wpa_supplicant-nl80211-wlan0.conf ${D}${sysconfdir}/wpa_supplicant/
}