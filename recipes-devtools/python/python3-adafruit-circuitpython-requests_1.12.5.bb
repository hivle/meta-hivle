SUMMARY = "A requests-like library for web interfacing"
HOMEPAGE = "https://github.com/adafruit/Adafruit_CircuitPython_Requests"
SECTION = "devel/python"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=66e022c238fbc8444a4a5166aed6ccb2"

inherit pypi setuptools3

DEPENDS += " ${PYTHON_PN}-setuptools-scm-native"

SRC_URI[md5sum] = "b03991f927df93a1877e77c09b78fd46"
SRC_URI[sha256sum] = "0f9af14be73b4ceb32b64aa6c2d832be1776a5e39a2a4e85618a4e6858b7996a"

RDEPENDS:${PN} = " \
    ${PYTHON_PN}-adafruit-blinka \
"