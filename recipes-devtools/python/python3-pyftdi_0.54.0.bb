DESCRIPTION = "FTDI device driver (pure Python)"
HOMEPAGE = "https://github.com/eblot/pyftdi"
SECTION = "devel/python"
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/BSD-3-Clause;md5=550794465ba0ec5312d6919e203a55f9"

inherit pypi setuptools3

SRC_URI[md5sum] = "d8969beb9cd11c123f1249963bf8c0d8"
SRC_URI[sha256sum] = "8df9af22077d17533d2f95b508b1d87959877627ea5dc2369056e90a3b5a232d"

RDEPENDS:${PN} = " \
    ${PYTHON_PN}-pyusb \
    ${PYTHON_PN}-pyserial \
"