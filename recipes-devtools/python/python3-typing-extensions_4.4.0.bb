SUMMARY = "Backported and Experimental Type Hints for Python 3.7+"
HOMEPAGE = "https://github.com/python/typing_extensions"
SECTION = "devel/python"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=f16b323917992e0f8a6f0071bc9913e2"

inherit pypi python_flit_core

PYPI_PACKAGE = "typing_extensions"

SRC_URI[md5sum] = "5cfcb56ea6fc4972c3600c0030f4d136"
SRC_URI[sha256sum] = "1511434bb92bf8dd198c12b1cc812e800d4181cfcb867674e0f8279cc93087aa"
