SUMMARY = "CircuitPython bus device classes to manage bus sharing."
HOMEPAGE = "https://github.com/adafruit/Adafruit_CircuitPython_BusDevice"
SECTION = "devel/python"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=6ec69d6e9e6c85adfb7799d7f8cf044e"

inherit pypi setuptools3

DEPENDS += " ${PYTHON_PN}-setuptools-scm-native"

SRC_URI[md5sum] = "deca4962bf47ad677c0570296bf614d3"
SRC_URI[sha256sum] = "9cc5d532d85b04f100fc7bd108b25fa1865e55dbc92dd8b6f2c57e7ef143da11"

RDEPENDS:${PN} = " \
    ${PYTHON_PN}-adafruit-blinka \
    ${PYTHON_PN}-adafruit-circuitpython-typing \
"
