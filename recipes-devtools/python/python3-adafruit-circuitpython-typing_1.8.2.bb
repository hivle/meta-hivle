SUMMARY = "Types needed for type annotation that are not in `typing`"
HOMEPAGE = "https://github.com/adafruit/Adafruit_CircuitPython_Typing"
SECTION = "devel/python"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=a089cc2176ad7f6066833cbef57695b0"

inherit pypi python_setuptools_build_meta

DEPENDS += " ${PYTHON_PN}-setuptools-scm-native"

SRC_URI[md5sum] = "061466fab2966147aa1eebfe67e33efd"
SRC_URI[sha256sum] = "8486951d9f105ca14670c6a237c6e2dd6a533fddcb4277b3aa33f3d08b8f8783"

RDEPENDS:${PN} = " \
    ${PYTHON_PN}-adafruit-blinka \
    ${PYTHON_PN}-adafruit-circuitpython-busdevice \
    ${PYTHON_PN}-adafruit-circuitpython-requests \
    ${PYTHON_PN}-typing-extensions \
"
