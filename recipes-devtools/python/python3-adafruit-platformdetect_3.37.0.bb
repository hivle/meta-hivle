SUMMARY = "Platform detection for use by libraries like Adafruit-Blinka."
HOMEPAGE = "https://github.com/adafruit/Adafruit_Python_PlatformDetect"
SECTION = "devel/python"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=fccd531dce4b989c05173925f0bbb76c"

inherit pypi setuptools3

PYPI_PACKAGE = "Adafruit-PlatformDetect"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"
SRC_URI += "file://0001-add-nanopi-neo-board-support.patch"

DEPENDS += " ${PYTHON_PN}-setuptools-scm-native"

SRC_URI[md5sum] = "978a1ce4db9a79c2d6deea5d4894cf84"
SRC_URI[sha256sum] = "be1071fcd001383da96ad073235e5766a6d34ed6dfdeb4d4203c75b1883ec988"
