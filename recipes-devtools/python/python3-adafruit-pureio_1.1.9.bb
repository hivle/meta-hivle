SUMMARY = "Pure python (i.e. no native extensions) access to Linux IO including I2C and SPI. Drop in replacement for smbus and spidev modules."
HOMEPAGE = "https://github.com/adafruit/Adafruit_Python_PureIO"
SECTION = "devel/python"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=2a21fcca821a506d4c36f7bbecc0d009"

inherit pypi setuptools3

PYPI_PACKAGE = "Adafruit_PureIO"

DEPENDS += " ${PYTHON_PN}-setuptools-scm-native"

SRC_URI[md5sum] = "f7d2f93db567333321b4097b4858bea5"
SRC_URI[sha256sum] = "2caf22fb07c7f771d83267f331a76cde314723f884a9570ea6f768730c87a879"

RDEPENDS:${PN} += " \
    python3-core \
    python3-ctypes \
    python3-fcntl \
"