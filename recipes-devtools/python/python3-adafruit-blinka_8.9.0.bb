DESCRIPTION = "CircuitPython APIs for non-CircuitPython versions of Python such as CPython on Linux and MicroPython."
HOMEPAGE = "https://github.com/adafruit/Adafruit_Blinka"
SECTION = "devel/python"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=fccd531dce4b989c05173925f0bbb76c"

inherit pypi setuptools3

PYPI_PACKAGE = "Adafruit-Blinka"

DEPENDS += " ${PYTHON_PN}-setuptools-scm-native"

SRC_URI[md5sum] = "583d7b4068870d07a21026f845511c5a"
SRC_URI[sha256sum] = "37614bbfd8a00f512c6b84ded051e363c5462436b748c7e0bc448ebb8c02b184"

# python() {
#     import re
#     machine = list(filter(re.compile("^raspberrypi").match, [i for i in d]))
#     if not machine:
#         bb.build.addtask('do_clean_prebuilds', 'do_package', 'do_install', d)
# }

do_install:append() {
    # Package ships with libgpiod_pulsein and libgpiod_pulsein64
    # with are prebuilds. This should only be on 32bit RPi machines
    # based on bcm283x.
    rm -rf ${D}${PYTHON_SITEPACKAGES_DIR}/adafruit_blinka/microcontroller/bcm283x
    rm -rf ${D}${PYTHON_SITEPACKAGES_DIR}/adafruit_blinka/microcontroller/amlogic/a311d/
}

RDEPENDS:${PN} = " \
    libgpiod \
    ${PYTHON_PN}-gpiod \
    ${PYTHON_PN}-adafruit-platformdetect \
    ${PYTHON_PN}-adafruit-pureio \
    ${PYTHON_PN}-pyftdi \
    ${PYTHON_PN}-adafruit-circuitpython-typing \
"
