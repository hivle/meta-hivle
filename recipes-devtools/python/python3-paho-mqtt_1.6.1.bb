DESCRIPTION = "MQTT version 5.0/3.1.1 client class"
HOMEPAGE = "https://github.com/eclipse/paho.mqtt.python"
SECTION = "devel/python"
LICENSE = "EPL-2.0"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=8e5f264c6988aec56808a3a11e77b913"

inherit pypi setuptools3

SRC_URI[md5sum] = "bdb20f88db291fdb4a0fe804c0f29316"
SRC_URI[sha256sum] = "2a8291c81623aec00372b5a85558a372c747cbca8e9934dfe218638b8eefc26f"
