SUMMARY = "Python USB access module"
HOMEPAGE = "https://pyusb.github.io/pyusb"
SECTION = "devel/python"
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://LICENSE;md5=e64a29fcd3c3dd356a24e235dfcb3905"

inherit pypi python_setuptools_build_meta

DEPENDS += " ${PYTHON_PN}-setuptools-scm-native"

SRC_URI[md5sum] = "880008dff32dac8f58076b4e534492d9"
SRC_URI[sha256sum] = "a4cc7404a203144754164b8b40994e2849fde1cfff06b08492f12fff9d9de7b9"
