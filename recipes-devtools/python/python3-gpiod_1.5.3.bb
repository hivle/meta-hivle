SUMMARY = "Pure Python implementation of libgpiod."
HOMEPAGE = "https://github.com/hhk7734/python3-gpiod"
SECTION = "devel/python"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=fc4e57275d8c245894e576844466f725"

inherit pypi setuptools3

SRC_URI[md5sum] = "d5467a7bd91834a42e546fa036ff0d87"
SRC_URI[sha256sum] = "35c76009800a715ede673a8ec2b60d426850cc158dfb7c34d937caf197b470db"
