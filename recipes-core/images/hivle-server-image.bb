SUMMARY = "Image for the IoT Hivle server"
LICENSE = "MIT"

inherit core-image

require hivle-core-image.bb

CORE_IMAGE_EXTRA_INSTALL += " \
    hivle-server-packagegroup \
"
