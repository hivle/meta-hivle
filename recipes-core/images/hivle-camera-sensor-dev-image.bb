SUMMARY = "Image for the IoT Hivle Camera Sensor"
LICENSE = "MIT"

inherit core-image

require hivle-camera-sensor-image.bb

EXTRA_IMAGE_FEATURES = "debug-tweaks empty-root-password allow-empty-password"

CORE_IMAGE_EXTRA_INSTALL += " \
    dev-packagegroup \
"
