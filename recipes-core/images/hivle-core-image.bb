SUMMARY = "Minimal Hivle core image"
LICENSE = "MIT"

inherit core-image

IMAGE_INSTALL:append = " packagegroup-core-boot"
IMAGE_LINGUAS = "pl-pl en-us"

CORE_IMAGE_EXTRA_INSTALL += " \
    buildtools-packagegroup \
    connectivity-packagegroup \
    python-packagegroup \
    resize-disk \
"