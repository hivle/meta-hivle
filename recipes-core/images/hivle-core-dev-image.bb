SUMMARY = "Minimal Hivle core development image"
LICENSE = "MIT"

inherit core-image

require hivle-core-image.bb
EXTRA_IMAGE_FEATURES = "debug-tweaks empty-root-password allow-empty-password"

CORE_IMAGE_EXTRA_INSTALL += " \
    dev-packagegroup \
"
