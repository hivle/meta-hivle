SUMMARY = "Image for the IoT Hivle server"
LICENSE = "MIT"

inherit core-image

require hivle-server-image.bb

EXTRA_IMAGE_FEATURES = "debug-tweaks empty-root-password allow-empty-password"

CORE_IMAGE_EXTRA_INSTALL += " \
    buildtools-packagegroup-dev \
    dev-packagegroup \
    python-packagegroup-dev \
"