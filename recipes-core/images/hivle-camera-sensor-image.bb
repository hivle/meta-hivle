SUMMARY = "Image for the IoT Hivle Camera Sensor"
LICENSE = "MIT"

inherit core-image

require hivle-core-image.bb

CORE_IMAGE_EXTRA_INSTALL += " \
    hivle-camera-sensor-packagegroup \
"
