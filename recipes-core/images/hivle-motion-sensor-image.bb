SUMMARY = "Image for the IoT Hivle Motion Sensor"
LICENSE = "MIT"

inherit core-image

require hivle-core-image.bb

CORE_IMAGE_EXTRA_INSTALL += " \
    hivle-motion-sensor-packagegroup \
"
