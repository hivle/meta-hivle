DESCRIPTION = "Group of packages used in building software"
SUMMARY = "Buildtools packages"
LICENSE = "MIT"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

RDEPENDS:${PN} = " \
    autoconf \
    automake \
    binutils \
    binutils-symlinks \
    cpp \
    cpp-symlinks \
    gcc \
    gcc-symlinks \
    g++ \
    g++-symlinks \
    gettext \
    make \
    libstdc++ \
    libtool \
    pkgconfig \
"

RDEPENDS:${PN}-dev = " \
    libstdc++-dev \
"