DESCRIPTION = "Group of packages to allow comunication with the board"
SUMMARY = "Connectivity utilities"
LICENSE = "MIT"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

RDEPENDS:${PN} = " \
    curl \
    dropbear \
    i2c-tools \
    wpa-supplicant \
    wget \
"