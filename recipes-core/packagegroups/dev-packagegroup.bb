DESCRIPTION = "Group of packages used in developing software"
SUMMARY = "Dev packages"
LICENSE = "MIT"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

RDEPENDS:${PN} = " \
    git \
    htop \
    systemd-analyze \
    vim \
"