DESCRIPTION = "Hivle IoT Camera Sensor applications"
SUMMARY = "Hivle IoT Camera Sensor applications"
LICENSE = "MIT"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

RDEPENDS:${PN} = " \
    gstreamer1.0 \
    gstreamer1.0-rtsp-server \
    libcamera \
    libexif \
    jpeg \
    tiff \
    libpng \
    boost \
    libcamera-apps \
    rtsp-simple-server \
"
