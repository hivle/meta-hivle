DESCRIPTION = "Hivle IoT Motion Sensor applications"
SUMMARY = "Hivle IoT Motion Sensor applications"
LICENSE = "MIT"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

RDEPENDS:${PN} = " \
    python3-hivle-motion-sensor \
"
