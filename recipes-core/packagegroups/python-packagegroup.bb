DESCRIPTION = "Group of packages connected to scripting in python"
SUMMARY = "Python packages"
LICENSE = "MIT"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

RDEPENDS:${PN} = " \
    python3 \
    libpython3 \
    python3-core \
    python3-pip \
    python3-wheel \
    python3-venv \
"

RDEPENDS:${PN}-dev = " \
    python3-dev \
"