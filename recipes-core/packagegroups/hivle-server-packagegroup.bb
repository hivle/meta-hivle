DESCRIPTION = "Hivle IoT Home Assistant server applications"
SUMMARY = "Hivle server applications"
LICENSE = "MIT"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

RDEPENDS:${PN} = " \
    mosquitto \
    docker \
    connman \
    connman-client \
    homeassistant \
    wireguard-tools \
"
