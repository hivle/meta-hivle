DESCRIPTION = "PIR motion sensor device to use with Hivle IoT infrastructure"
HOMEPAGE = "https://gitlab.com/jakub.kozlowicz/hivle-motion-sensor"
SECTION = "devel/python"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=c0f24fc03b78d8635b1d5e5a2a147064"

# To be set in local.conf
MQTT_HIVLE_USER ?= ""
MQTT_HIVLE_PASSWORD ?= ""
MQTT_HIVLE_HOST ?= ""
MQTT_HIVLE_PORT ?= ""

inherit python_poetry_core systemd

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " \
    file://config.json \
    file://hivle-motion-sensor.service \
    https://gitlab.com/api/v4/projects/41653717/packages/pypi/files/067e40a9463918741fd7c3bfe1b0c2158295fa1f78f589749020389c127c428b/hivle_motion_sensor-${PV}.tar.gz \
    file://0001-remove-groups-from-poetry.patch \
"

SYSTEMD_SERVICE:${PN}:append = " hivle-motion-sensor.service"
SYSTEMD_AUTO_ENABLE = "enable"

S = "${WORKDIR}/hivle_motion_sensor-${PV}"

SRC_URI[md5sum] = "4903f2ce2dffed8f77ccf8114e5aa295"
SRC_URI[sha256sum] = "067e40a9463918741fd7c3bfe1b0c2158295fa1f78f589749020389c127c428b"

do_install:append() {
    install -d ${D}${sysconfdir}/hivle/
    install -D -m 0644 ${WORKDIR}/config.json ${D}${sysconfdir}/hivle/

    sed -i 's/%MQTT_HIVLE_USER/${MQTT_HIVLE_USER}/g' ${D}${sysconfdir}/hivle/config.json
    sed -i 's/%MQTT_HIVLE_PASSWORD/${MQTT_HIVLE_PASSWORD}/g' ${D}${sysconfdir}/hivle/config.json
    sed -i 's/%MQTT_HIVLE_HOST/${MQTT_HIVLE_HOST}/g' ${D}${sysconfdir}/hivle/config.json
    sed -i 's/%MQTT_HIVLE_PORT/${MQTT_HIVLE_PORT}/g' ${D}${sysconfdir}/hivle/config.json

    install -d ${D}${systemd_system_unitdir}
    install -D -m 0644 ${WORKDIR}/hivle-motion-sensor.service ${D}${systemd_system_unitdir}/
}

FILES:${PN} += " \
    ${sysconfdir}/hivle/config.json \
    ${systemd_system_unitdir}/hivle-motion-sensor.service \
"

RDEPENDS:${PN} += " \
    ${PYTHON_PN}-adafruit-blinka \
    ${PYTHON_PN}-paho-mqtt \
"
