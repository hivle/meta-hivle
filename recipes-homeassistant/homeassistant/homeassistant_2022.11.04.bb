SUMMARY = "Open-source home automation platform running on Python 3"
HOMEPAGE = "https://home-assistant.io/"
SECTION = "devel/python"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit systemd

# Set this variables in local.conf
HIVLE_HA_EMAIL ?= ""
HIVLE_HA_PASSWORD ?= ""
HIVLE_HA_RECIPIENT ?= ""
HIVLE_HA_TELEGRAM_ID ?= ""
HIVLE_HA_TELEGRAM_API ?= ""

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SYSTEMD_SERVICE:${PN}:append = " homeassistant.service"
SYSTEMD_AUTO_ENABLE = "enable"

SRC_URI += " \
    file://homeassistant.service \
    file://automations.yaml \
    file://configuration.yaml \
    file://notifiers.yaml \
    file://scenes.yaml \
    file://scripts.yaml \
    file://secrets.yaml \
"

FILES:${PN} += " \
    ${sysconfdir}/homeassistant/* \
    ${systemd_system_unitdir}/homeassistant.service \
"

do_install:append() {
    install -d ${D}${sysconfdir}/homeassistant
    install -D -m 0644 ${WORKDIR}/automations.yaml ${D}${sysconfdir}/homeassistant/
    install -D -m 0644 ${WORKDIR}/configuration.yaml ${D}${sysconfdir}/homeassistant/
    install -D -m 0644 ${WORKDIR}/notifiers.yaml ${D}${sysconfdir}/homeassistant/
    install -D -m 0644 ${WORKDIR}/scenes.yaml ${D}${sysconfdir}/homeassistant/
    install -D -m 0644 ${WORKDIR}/scripts.yaml ${D}${sysconfdir}/homeassistant/
    install -D -m 0644 ${WORKDIR}/secrets.yaml ${D}${sysconfdir}/homeassistant/

    sed -i 's/%HIVLE_HA_EMAIL/${HIVLE_HA_EMAIL}/g' ${D}${sysconfdir}/homeassistant/secrets.yaml
    sed -i 's/%HIVLE_HA_PASSWORD/${HIVLE_HA_PASSWORD}/g' ${D}${sysconfdir}/homeassistant/secrets.yaml
    sed -i 's/%HIVLE_HA_RECIPIENT/${HIVLE_HA_RECIPIENT}/g' ${D}${sysconfdir}/homeassistant/secrets.yaml
    sed -i 's/%HIVLE_HA_TELEGRAM_ID/${HIVLE_HA_TELEGRAM_ID}/g' ${D}${sysconfdir}/homeassistant/secrets.yaml
    sed -i 's/%HIVLE_HA_TELEGRAM_API/${HIVLE_HA_TELEGRAM_API}/g' ${D}${sysconfdir}/homeassistant/secrets.yaml

    install -d ${D}${systemd_system_unitdir}
    install -D -m 0644 ${WORKDIR}/homeassistant.service ${D}${systemd_system_unitdir}/homeassistant.service
}

RDEPENDS:${PN} += " \
    docker \
"