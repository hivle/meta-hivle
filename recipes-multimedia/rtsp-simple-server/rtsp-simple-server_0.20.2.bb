SUMMARY = "Ready-to-use RTSP / RTMP / LL-HLS server and proxy that allows to read, publish and proxy video and audio streams"
HOMEPAGE = "https://github.com/aler9/rtsp-simple-server" 
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://../LICENSE;md5=77fd2623bd5398430be5ce60489c2e81"

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI:append = " \
    file://rtsp-simple-server.yaml \
    file://rtsp-simple-server.service \
    https://github.com/aler9/rtsp-simple-server/releases/download/v${PV}/rtsp-simple-server_v${PV}_linux_armv7.tar.gz \
"

SRC_URI[sha256sum] = "382ef267c35b87285340a00877c1d61c96b24fc302ac923ae2f5f78fdb2dfdbf"

SRCREV = "4c96a6873e38ae92b58bbc38545fefa48bdba205"

inherit systemd

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE:${PN} = "rtsp-simple-server.service"

do_install:append() {
    install -d ${D}${bindir}
    install -m 0755 ${WORKDIR}/rtsp-simple-server ${D}${bindir}/

    install -d ${D}${sysconfdir}
    install -m 0644 ${WORKDIR}/rtsp-simple-server.yaml ${D}${sysconfdir}/

    install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/rtsp-simple-server.service ${D}${systemd_system_unitdir}/
}

FILES:${PN}:append = " \
    ${bindir}/rtsp-simple-server \
    ${sysconfdir}/rtsp-simple-server.yaml \
    ${systemd_system_unitdir}/rtsp-simple-server.service \
"
