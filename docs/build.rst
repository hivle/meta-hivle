Build
=====

Add meta-hivle layer
""""""""""""""""""""

.. code-block:: console

    bitbake-layers add-layer meta-hivle


Source environment
""""""""""""""""""

Use template files from the meta-hivle conf directory.

.. code-block:: console

    TEMPLATECONF=meta-hivle/conf source oe-init-build-env


Build minimal core image
""""""""""""""""""""""""

.. code-block:: console

    bitbake hivle-core-image


Optionally you can choose machine from this step and there is no need to change ``local.conf`` file.

.. code-block:: console

    MACHINE=libretech-cc bitbake hivle-core-image