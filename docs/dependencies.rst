Dependencies
============

meta-raspberrypi
   | URI: git://git.yoctoproject.org/meta-raspberrypi
   | branch: kirkstone

meta-sunxi
   | URI: https://github.com/linux-sunxi/meta-sunxi
   | branch: kirkstone

meta-meson
   | URI: https://github.com/superna9999/meta-meson
   | branch: kirkstone

meta-oe
   | URI: git://git.openembedded.org/meta-openembedded
   | branch: kirkstone
   | subdirectory: meta-oe

meta-python
   | URI: git://git.openembedded.org/meta-openembedded
   | branch: kirkstone
   | subdirectory: meta-python

meta-webserver
   | URI: git://git.openembedded.org/meta-openembedded
   | branch: kirkstone
   | subdirectory: meta-webserver

meta-networking
   | URI: git://git.openembedded.org/meta-openembedded
   | branch: kirkstone
   | subdirectory: meta-networking

meta-filesystems
   | URI: git://git.openembedded.org/meta-openembedded
   | branch: kirkstone
   | subdirectory: meta-filesystems

meta-virtualization
   | URI: git://git.yoctoproject.org/meta-virtualization
   | branch: kirkstone