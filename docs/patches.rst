Patches
=======

Please submit any patches via MR on gitlab repo https://gitlab.com/jakub.kozlowicz/meta-hivle

**Maintainers:**

- Jakub Kozłowicz <ja.kozlowicz@gmail.com>