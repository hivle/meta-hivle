.. include:: ../README.rst

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Contents:

   patches
   dependencies
   build


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
