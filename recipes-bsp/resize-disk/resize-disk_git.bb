SUMMARY = "Tools to resize disk size to match its original size"
SECTION = "devel"

LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0-or-later;md5=fed54355545ffd980b814dab4a3b312c"

SRC_URI = " \
    file://resize-helper \
    file://resize-disk \
    file://resize-helper.service \
"

FILES:${PN}:append = " \
    ${systemd_system_unitdir}/resize-helper.service \
    ${sbindir}/resize-helper \
    ${sysconfdir}/init.d/resize-disk \
"

inherit systemd allarch update-rc.d

do_install () {
    install -d ${D}${sysconfdir}/init.d
    install -m 0755 ${WORKDIR}/resize-disk ${D}${sysconfdir}/init.d/

    install -d ${D}${systemd_unitdir}/system
    install -m 0644 ${WORKDIR}/resize-helper.service ${D}${systemd_unitdir}/system

    install -d ${D}${sbindir}
    install -m 0755 ${WORKDIR}/resize-helper ${D}${sbindir}
}

INITSCRIPT_NAME = "resize-disk"
INITSCRIPT_PARAMS = "start 99 5 2 . stop 20 0 1 6 ."

SYSTEMD_SERVICE:${PN} = "resize-helper.service"
RDEPENDS:${PN} += "e2fsprogs-resize2fs gptfdisk parted util-linux udev"