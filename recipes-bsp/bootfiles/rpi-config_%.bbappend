
do_deploy:append() {
    echo "# Automatically load overlays for detected cameras" >> $CONFIG
    echo "camera_auto_detect=1" >> $CONFIG
}

do_deploy:append:raspberrypi0-2w() {
    echo "# have a properly sized image" >> $CONFIG
    echo "disable_overscan=1" >> $CONFIG

    echo "# Enable audio (loads snd_bcm2835)" >> $CONFIG
    echo "dtparam=audio=on" >> $CONFIG
}